(function() {
  angular.module('taxApp',
    ['ui.bootstrap', 'validator', 'validator.rules'])

    .config(['$validatorProvider',
      function($validatorProvider) {
         $validatorProvider.register('in_percentage_range', {
           invoke: 'watch',
           validator: function(value, scope) {
             if (0 <= value && value <= 100) {
               return true;
             } else {
               return false;
             }
           },
           error: '範圍必須介於 0 ~ 100'
         });
      }
    ])

  .controller('taxCtrl', ['$scope', '$validator',
      function($scope, $validator) {
        $scope.form_model = {
          last_date: null,
          price: null,
          last_price: null,
          rate: null,
          area: null,
          type: '都市土地',
          numerator: 1,
          denominator: 1,
        };

        $scope.result = null;

        $scope.getFormula = function(year, ratio) {
          var year_header  = ['year < 20', '20 <= year && year < 30', '30 <= year && year < 40', '40 <= year'],
              ratio_header = ['0 < ratio && ratio < 1', '1 <= ratio && ratio < 2', '2 <= ratio'],
              table = [
                ['a × 20％', 'a × 20％', 'a × 20％', 'a × 20％'],
                ['a × 30％ － b × 10％', 'a × 28％ － b × 8％', 'a × 27％ － b × 7％', 'a × 26％ － b × 6％'],
                ['a × 40％ － b × 30％', 'a × 36％ － b × 24％', 'a× 34％ － b × 21％', 'a × 32％ － b × 18％']
              ];

          for (var x = 0; x < year_header.length; x++) {
            if (eval(year_header[x])) {
              for (var y = 0; y < ratio_header.length; y++) {
                if (eval(ratio_header[y])) {
                  return table[y][x];
                }
              }
            }
          }
        };

        $scope.calculate = function() {
          for (var key in $scope.form_model) {
            if ($scope.form_model[key] == null) { return; }
          }

          var timeDiff = Math.abs((new Date()).getTime() - (new Date($scope.form_model.last_date)).getTime());
          var age = Math.floor(timeDiff / (365 * 1000 * 3600 * 24));

          var b = ($scope.form_model.last_price * $scope.form_model.area * $scope.form_model.rate / 100);
          var a = ($scope.form_model.price * $scope.form_model.area) - b;
          var benefit_ratio = a / b;

          var formula = $scope.getFormula(age, benefit_ratio);

          formula = formula.replace(/×/g, '*');
          formula = formula.replace(/％/g, ' / 100');
          formula = formula.replace(/－/g, '-');

          tax_a = eval(formula);
          tax_b = a * 0.1;

          $scope.result = tax_a + tax_b;
        };

        $scope.submit = function () {
          $validator.validate($scope, 'form_model').success(function() {
            $scope.calculate();
          });
        };


      }
    ]);

})();
